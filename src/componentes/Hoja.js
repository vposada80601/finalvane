import React from 'react';
import '../styles/Hoja.css';

function Hoja(props) {
    return (
       <div className='contenedor-Hoja'>
        <img 
        className='imagen-Hoja'
        src={require(`../imagen/foto-${props.imagen}.jpg`)}
        alt='foto de vane' />

        <div className='contenedor-texto-Hoja'>
            <p className='nombre-Hoja'><strong>{props.nombre}</strong></p>
            <p className='cargo-Hoja'>{props.cargo} </p>
            <p className='texto-Hoja'>{props.texto}</p>



            </div>
        </div>
    );
    
}

export default Hoja;
